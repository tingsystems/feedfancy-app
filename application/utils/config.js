export default {
    api: {
        host: 'https://mercadomovil.com.mx/api/v1/',
        projectId: '6cb93684-c528-46e3-afeb-f2aff6d8c6de',
        headers: { 
            Accept: 'application/json', 
            'Content-Type': 'application/json', 
            'PROJECT-ID': '6cb93684-c528-46e3-afeb-f2aff6d8c6de'
        },
    }
}