import t from 'tcomb-form-native';

export default formValidation = {
	email: t.refinement(t.String, (s) => {
		return /@/.test(s);
	}),
	password: t.refinement(t.String, (s) => {
		return s.length >= 6;
	}),
	ApiUtils: {
		checkStatus(response) {
		  if (response.status >= 200 && response.status < 300) {
			return response;
		  }
		  const error = new Error({ message: response._bodyText, status: response.status });
		  error.response = response;
		  throw error;
		},
		parseParams(patch, params) {
			let queryParams = patch;
			Object.keys(params).map((key, index) => {
				if (index === 0) {
					queryParams += `?${key}=${params[key]}`;
				} else {
					queryParams += `&${key}=${params[key]}`;
				}
				return queryParams;
			});
			return queryParams;
		}
	}
}