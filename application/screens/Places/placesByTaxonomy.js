import React, {Component} from 'react';
import { observer } from 'mobx-react/native';
import { Container, Content,List, ListItem, Text, Left, Body,Thumbnail, Right, Button } from 'native-base';
import {View, FlatList, TouchableHighlight, ActivityIndicator, Linking, StyleSheet } from 'react-native';
import { SearchBar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { NavigationActions } from 'react-navigation';
import stores from '../../stores';
import config from '../../utils/config';
import utils from '../../utils/validation';

@observer
export default class PlacesScreen extends Component {

  static navigationOptions = {
		title: 'Negocios'
	};

  constructor (props) {
		super(props);
    this.state= {
      working: true,
      places: [],
      params: {
        isActive: 'True',
        search: '',
        taxonomies: `${stores.menuStore.taxonomy.slug},${stores.menuStore.city.slug}`,
        fields: 'id,title,metadata,keywords,content,link,lng,lt,excerpt,attachments',
        page: 1,
        pageSize: 25
      }
    };
  }

  componentDidMount() {
    this.getPlaces(this.state.params);
  }

  goPlaceDetail( place ) {
    const navigateAction = NavigationActions.navigate({
		routeName: 'PlaceDetailScreen',
         params: { place }
	  });
	  this.props.navigation.dispatch(navigateAction);
  }

  handleSearch = (term) => {
		this.state.params.search = term;
		this.setState({ params: this.state.params }, () => {
			if (this.state.params.search.length >= 4) {
				this.state.params.page = 1;
				this.setState({ params: this.state.params }, () => {
					this.getPlaces(this.state.params);
				});
			} else if (this.state.params.search.length == 0) {
				this.state.params.page = 1;
				this.setState({ params: this.state.params }, () => {
          this.getPlaces(this.state.params);
				});
			}
		});
  }
  
  handleClearSearch = () => {
		this.state.params.page = 1;
		this.state.params.search = '';
		this.setState({ params: this.state.params }, 
			() => {
        this.getPlaces(this.state.params);
			}
		);
  }
  
  handleLoadMore = () => {
		if (this.state.next) {
			this.state.params.page += 1
			this.setState({params: this.state.params},
		  		() => {
            this.getPlaces(this.state.params);
		  		}
			);
		}
  }
  
  renderHeader = () => {
		return <SearchBar placeholder="Buscar" lightTheme round 
          onChangeText={(term) => this.handleSearch(term)}
				/>;
  }

  renderFooter = () => {
		if (!this.state.working) return null;
	
		return (
		  <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
		  >
			<ActivityIndicator animating size="large" />
		  </View>
		);
	}

  renderBuss( place ) {
    return(
      <ListItem thumbnail style={styles.listItems}>
          <TouchableHighlight onPress={() => this.goPlaceDetail(place.item)}>
            <Left style={{ alignItems:"center" }}>
              {place.item.attachments.length ? <Thumbnail square source={{ uri: place.item.attachments[0].url }} style={{ height:130, width:130 }} />: 
              <Thumbnail square source={require('../../../assets/default-image.png')} style={{ height:130, width:130 }} />}
            </Left>
          </TouchableHighlight>
          <Body style={styles.bodyList}>
            <Text onPress={() => this.goPlaceDetail(place.item)} style={{ fontSize:18 }}>{place.item.title}.</Text>
            <Text note style={styles.textNote}>
              {stores.menuStore.taxonomy.name} | {stores.menuStore.city.name}
            </Text>
            <Text onPress={() => this.goPlaceDetail(place.item)} note>{place.item.excerpt}</Text>
          </Body> 
          <Right style={styles.rightList}>
            <Button transparent onPress={() => Linking.openURL(`tel:${place.item.metadata.phone}`)}>
              <Icon style={styles.actionCall} name="phone"/>
            </Button>
          </Right>
      </ListItem>);
  }

  render () { 
    const { places } = this.state
		return (
			<Container style={{padding:0, margin:0}}> 
        <Content>
          <List>
            <FlatList
              data={places}
              renderItem={(place) => this.renderBuss(place)}
              keyExtractor={(place) => place.id}
              ListHeaderComponent={this.renderHeader}
						  ListFooterComponent={this.renderFooter}
						  onEndReached={this.handleLoadMore}
          		onEndReachedThreshold={0.5}
            />
          </List>
        </Content>
      </Container>
    );
  }

  getPlaces(querySearch) {
    this.setState({working: true});
    const uri = utils.ApiUtils.parseParams(`${config.api.host}posts`, querySearch);
    fetch(uri, {
        method: 'get',
        headers: config.api.headers
    })
    .then(response => response.json())
    .then((response) => {
        this.setState({
            working: false, 
            places: response.results,
        });
    });
  }
}

const styles = StyleSheet.create({
	actionCall: { 
    fontSize: 20,
    color: '#1993de',
  },
  textNote: {
    fontSize:12,
    marginTop: 5,
    marginBottom: 5,
  },
  listItems: {
    paddingLeft:0,
    marginLeft:0,
    marginBottom: 5,
    
  },
  bodyList: {
    height: 130,
  },
  rightList: {
    height: 130,
  }
});