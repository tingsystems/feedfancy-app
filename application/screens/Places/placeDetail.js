import React, {Component} from 'react';
import { observer } from 'mobx-react/native';
import {Image, Dimensions, TouchableHighlight, Linking, StyleSheet} from 'react-native';
import { Container, Card, CardItem, Content, List, ListItem, Text, Right, Body} from 'native-base';
import HTML from 'react-native-render-html';
import Icon from 'react-native-vector-icons/FontAwesome';

import stores from '../../stores';

var { height } = Dimensions.get('window');
var { width } = Dimensions.get('window');
var h= height / 2;

const number = {
  number: '9093900003', // String value with the number to call
  prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}

@observer
export default class PlaceDetailScreen extends Component {

  constructor (props) {
		super(props);
    const { params } = props.navigation.state;
    this.state= {
      place : params.place
    };
  }

    render () { 
      const { place } = this.state;
      const address = `${place.metadata.street} ${stores.menuStore.city.name}`;
		  return (
			  <Container style={{padding:0, margin:0}}> 
          <Content>
            <List>
              <ListItem style={{marginLeft:0, paddingLeft:0}}>
                <Body style={{marginLeft:0, paddingLeft:0}}>
                  <Text style={{fontSize:20}}>{ place.title }</Text>
                  <Text note style={{fontSize:11}}>{stores.menuStore.city.name}, Mich</Text>
                </Body>
                <Right>
                    <TouchableHighlight underlayColor="#fff" onPress={() => Linking.openURL(`tel:${place.metadata.phone}`)}>
                      <Text style={{fontSize:14, color:"#1993de"}}><Icon name="phone" size={15} />  LLAMAR</Text>
                    </TouchableHighlight>
                </Right>
              </ListItem>
            </List>
            <Card style={{ paddingBottom:0, paddingTop:0, paddingLeft:0, marginLeft:0, marginTop:0, marginBottom:0, margin:0, padding:0, borderColor:'#f2f2f2',borderStartWidth:0, borderWidth:0, shadowColor:'#f2f2f2', shadowOpacity:0, shadowRadius:0 }}>
            <CardItem cardBody>
              <Body style={{alignItems:"center"}}>
                <Image source={{ uri: place.attachments[0].url  }} style={{ height: h, width:width}}/>
              </Body>
            </CardItem>
          </Card>
          <List transparent style={{padding:0, margin:0, marginLeft:0}}>
            <ListItem itemDivider style={{marginLeft:0}}>
              <Text note style={{marginLeft:10}}>Datos de contacto</Text>
            </ListItem>                    
            <ListItem onPress={() => Linking.openURL(`tel:${place.metadata.phone}`)}  style={{alignItems:"center", marginLeft:0}}>
              <Body>
                <Text>Teléfono.</Text>
                <Text style={{color: "#1993de", marginTop: 5}}>
                  {place.metadata.phone}
                </Text>
              </Body> 
            </ListItem>
            <ListItem  onPress={() => Linking.openURL(`mailto: support@expo.io`)} style={[styles.listItem]}>
              <Body>
                <Text note>Correo</Text>
                <Text style={{color: "#1993de", marginTop: 5}}>{place.metadata.email}</Text>
              </Body>
            </ListItem>
            <ListItem 
              style={[styles.listItem]} 
              onPress={() =>  Linking.openURL(`https://www.google.com.mx/maps/dir//${address}`)}
            >
              <Body>
                <Text note >Dirección</Text>
                <Text style={{color: "#1993de", marginTop: 5}}>
                  {place.metadata.street} Col. {place.metadata.neighborhood} C.P. {place.metadata.zip}
                </Text>
              </Body>
            </ListItem>
            <ListItem itemDivider>
              <Text note>Descripción</Text>
            </ListItem>                    
            <ListItem style={{alignItems:"center"}}>
              <HTML html={place.content} imagesMaxWidth={Dimensions.get('window').width} />
            </ListItem>
          </List>
        </Content>
        </Container>);
    }
}

const styles = StyleSheet.create({
	listItem: {
    alignItems:"center", 
    marginLeft:0
  },
});