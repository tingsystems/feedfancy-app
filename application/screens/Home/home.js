import React, {Component} from 'react';
import { StyleSheet, FlatList, Image } from 'react-native';
import { Container, List, Text, Left, Card, CardItem } from 'native-base'
import { NavigationActions } from 'react-navigation';
import { observer } from 'mobx-react/native';
import utils from '../../utils/validation';
import config from '../../utils/config';
import stores from '../../stores';

@observer
export default class HomeScreen extends Component {

	static navigationOptions = {
		title: 'Directorio Comercial'
	};

	constructor() {
		super();
		this.state = {
			params: {
                isActive: 'True',
                ordering: 'name',
                kind: 'city',
                fields: 'id,name,slug,children,parent,metadata,icon'
            },
			cities: [],
		}
	}
	
	componentDidMount() {
		this.getTaxonomies(this.state.params);
	}
    
	goMainMenu (city) {
        stores.menuStore.setCity(city)
		const navigateAction = NavigationActions.navigate({
			routeName: 'MainMenuScreen',
		});
		this.props.navigation.dispatch(navigateAction);
	}

	renderCity(city) {
		return (
			<Card
				style={{marginLeft: 20, marginRight:25, marginTop:20}}
			>
				<CardItem cardBody button onPress={() => this.goMainMenu(city.item)}>
					<Image source={{uri: city.item.metadata.image}} style={{height: 150, width: null, flex: 1}}/>
				</CardItem>
				<CardItem style={styles.cardStyles}>
					<Left>
						<Text style={styles.textTitle}>{city.item.name}</Text>
					</Left>
				</CardItem>
			</Card>
		);
	}

	render () {
		const {  cities } = this.state;
		return (
			<Container style={styles.backgroundContainer}>
				<List>
					<FlatList
						data={cities}
						renderItem={(city) => this.renderCity(city)}
						keyExtractor={city => city.id}
					/>
				</List>
			</Container>
		); 
	}

	getTaxonomies(querySearch) {
        this.setState({working: true});
		const uri = utils.ApiUtils.parseParams(`${config.api.host}taxonomies`, querySearch);
		fetch(uri, {
			method: 'get',
			headers: config.api.headers
		})
		.then(response => response.json())
		.then((response) => {
			this.setState({
				working: false, 
				cities: response,
			});
		});
	}
}

const styles = StyleSheet.create({
	cardStyles: {
		paddingTop: 10,
		paddingBottom: 10,
	},
    textTitle: {
		fontSize: 18,
		fontWeight: 'bold',
		color: '#555',
	},
    backgroundContainer: {
		backgroundColor: '#f5f5f5'
	}
});


