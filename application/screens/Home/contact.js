import React, {Component} from 'react';
import {Image, Dimensions, TouchableHighlight, Linking, StyleSheet} from 'react-native'
import { Container, Card, CardItem, Content, List, ListItem, Text, Left, Right, Body,Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';


const number = {
  number: '9093900003', // String value with the number to call
  prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}

//revisar ortografia (acentos)
export default class ContactScreen extends Component {

  static navigationOptions = {
		title: 'Contacto'
	};

  constructor (props) {
		super(props);
  }

    render () { 
	    return (
        <Container style={{padding:0, margin:0}}> 
          <Content>
            <Card transparent>
              <CardItem header style={styles.headerCard}>
                <Text style={styles.textCustom}>Directorio Regional Los Reyes, Periban, Santa Clara, Tinguindin</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <Text style={[styles.textCustom, styles.title]}>Informes y ventas</Text>
                  <Text style={styles.textCustom} onPress={() => Linking.openURL(`tel:3541055131`)}><Icon name="phone" size={18}/> 542 64 91</Text>
                  <Text style={styles.textCustom} onPress={() => Linking.openURL(`tel:3541055131`)}><Icon name="mobile" size={18}/> 354 105 51 31</Text>
                  <Text style={styles.textCustom} onPress={() => Linking.openURL(`mail:directoriolosreyes@hotmail.com`)}><Icon name="envelope" size={18}/> directoriolosreyes@hotmail.com</Text>
                </Body>
              </CardItem>
            </Card>
          </Content>
        </Container>
      );
    }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  headerCard: {
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
  },
	textCustom: {
    marginTop: 5,
    marginBottom: 5,
    color: '#666'
	},
    textTitle: {
		fontSize: 18,
		fontWeight: 'bold',
		color: '#555',
	},
    backgroundContainer: {
		backgroundColor: '#f5f5f5'
	}
});
