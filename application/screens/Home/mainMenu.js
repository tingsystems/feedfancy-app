import React, {Component} from 'react';
import { observer } from 'mobx-react/native'
import {StyleSheet, TouchableHighlight, Text, FlatList} from 'react-native';
import { Card, Icon, Container, Content, CardItem, Body } from 'native-base';
import config from '../../utils/config';
import { Col, Grid } from 'react-native-easy-grid';
import { NavigationActions } from 'react-navigation';
import PreLoader from '../../components/PreLoader';
import utils from '../../utils/validation';
import stores from '../../stores';

@observer
export default class MainMenuScreen extends Component {

    static navigationOptions = {
		title: 'Índice'
	};

    constructor() {
        super();
		this.state = {
            working: false,
            taxonomies: [],
            subtaxl: [],
            subtaxr: [],
            sendtax: [],
            selected: 0,
            titles: "Agencias",
            error: null,
            params: {
                isActive: 'True',
                ordering: 'name',
                parent: '353c2ac9-16e3-4d92-9fff-3cca3aa8ba30',
                fields: 'id,name,slug,children,parent,metadata,icon'
            }
		}
    }

    componentDidMount() {
        this.getTaxonomies(this.state.params);
    }

    renderTaxonomy (taxonomy) {
        return (
            <TouchableHighlight 
                onPress={() => this.getSubTax(taxonomy.item.children, taxonomy.index)} underlayColor="#ffffff"
            >
            <Card transparent style={{paddingBottom:0, paddingTop:0, marginTop:1, marginBottom:1, borderColor:'#f2f2f2', borderWidth:0, shadowColor:'transparent', borderRadius: 0}}>
                <CardItem 
                    style={{borderRadius: 0, backgroundColor: this.state.selected == taxonomy.index  ? '#ddd' : '#f2f2f2' }}
                >
                    <Body style={{alignItems:"center"}}>
                        <Icon
                            name={taxonomy.item.metadata.icon}
                            type="FontAwesome"
                            style={{ color:'#757575', paddingTop: 5, paddingBottom: 10, fontSize:20 }}
                            onPress={() => this.getSubTax(taxonomy.item.children, taxonomy.index)} />
                        <Text style={{ fontSize:12, textAlign: "center", paddingBottom:0, marginBottom:0 }}
                        onPress={() => this.getSubTax(taxonomy.item.children, taxonomy.index)} >
                            {taxonomy.item.name}
                        </Text>
                    </Body>
                </CardItem>
            </Card>
            </TouchableHighlight>
        );
    }

    renderSubax(subtax) {
        return (
            <TouchableHighlight onPress={() => this.sendBussi(subtax.item)} underlayColor="#f2f2f2">
                <Card transparent style={{ paddingBottom:0, paddingTop:0, paddingLeft:0, marginLeft:0, marginTop:0, marginBottom:0, margin:0, padding:0, borderColor:'#f2f2f2',borderStartWidth:0, borderWidth:0, shadowColor:'#f2f2f2', shadowOpacity:0, shadowRadius:0 }}>
                    <CardItem style={{backgroundColor:'#ddd' ,paddingLeft:0, paddingRight:0, borderColor:"#ddd", marginBottom: 3 }}>
                            <Body style={{alignItems:"center" }}>
                                {subtax.item.metadata ? 
                                <Icon style={styles.itemsStyles} type={subtax.item.metadata.source}  name={subtax.item.metadata.icon} /> :
                                <Icon style={styles.itemsStyles} type="FontAwesome"  name="houzz" />}
                                <Text style={styles.cardTitle}>
                                    {subtax.item.name}
                                </Text>
                            </Body>
                    </CardItem>
                </Card>
            </TouchableHighlight>
        );
    }

    render () { 
        const { working, taxonomies, subtaxl, subtaxr, selected } = this.state;

        if (working) {
            return (<PreLoader/>)
        }
        return (
            <Container style={styles.container}> 
                <Col style={ { width: 95, backgroundColor: '#d6d6d6', paddingLeft:0, marginLeft:0 } }>
                    <FlatList style={{margin:0}}
                        key={selected}
                        data={taxonomies}
                        renderItem={(item) => this.renderTaxonomy(item)}
                        keyExtractor={(item) => item.id}
                    />
                </Col>
                <Content>
                    <Grid padder>
                        <Col style={styles.cardContent}>
                            <FlatList
                                data={subtaxl}
                                renderItem={(item) => this.renderSubax(item)}
                                keyExtractor={(item) => item.id}
                            />
                        </Col>
                        <Col style={styles.cardContent}>
                            <FlatList
                                data={subtaxr}
                                renderItem={(item) => this.renderSubax(item)}
                                keyExtractor={(item) => item.id}
                            />
                        </Col>
                    </Grid>
                </Content>
            </Container>
        ); 
    }

    async getTaxonomies(querySearch) {
        this.setState({working: true});
        const uri = utils.ApiUtils.parseParams(`${config.api.host}taxonomies`, querySearch);
        fetch(uri, {
            method: 'get',
            headers: config.api.headers
        })
        .then(response => response.json())
        .then(async (response) => {
            this.setState({
                working: false, 
                taxonomies: response,
            }, async () => {
                this.getSubTax(response[0].children, 0);
            });
        });
    }

    sendBussi(taxonomy) {
        stores.menuStore.setTaxonomy(taxonomy);
        const navigateAction = NavigationActions.navigate({
			routeName: 'PlacesScreen',
		});
        this.props.navigation.dispatch(navigateAction);
    }

    getSubTax(subtax, selected) {
        let subtaxl = [];
        let subtaxr = [];
        subtax.map((t, i) => {
            if ( i % 2 === 0) {
                subtaxl.push(t);
            } else {
                subtaxr.push(t);
            }
        });
        this.setState({ selected, subtaxl, subtaxr });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ddd',
        margin: 0,
        padding: 0
    },
    con: {
        flex: 2,
        flexDirection: 'row'
    },
    box1: {
      flex: 2,
      backgroundColor: '#ddd',
    },
    box2: {
      flex: 7,
      backgroundColor: '#ddd'
    },
    menucont: {
        padding: 0, 
        marginLeft: 0, 
        backgroundColor: '#ffffff'
    },
    rescont: {
        padding: 0, 
        margin: 5, 
        backgroundColor: '#f2f2f2',
        borderRadius: 0,
        borderWidth: 0
    },
    cardFancy: {
        borderColor:'#ddd',
        borderStartWidth:0,
        borderWidth:0,
        shadowColor:'transparent',
        shadowOpacity:0,
        shadowRadius:0,
    },
    cardContent: {
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#ddd',
        borderRadius: 0
    },
    cardTitle: {
        fontSize:14,
        textAlign: 'center',
        paddingBottom:0,
        marginBottom:0,
        marginTop: 10,
        fontWeight: 'bold',
        color: '#666'
    },
    itemsStyles: {
        color: '#666',
    }
});
