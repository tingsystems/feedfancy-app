import React, {Component} from 'react';
import { TextInput, View, SafeAreaView, ScrollView, Image } from 'react-native';
import {createDrawerNavigator, createStackNavigator, DrawerItems  } from "react-navigation";
import { SearchBar } from 'react-native-elements'

import HomeScreen from '../screens/Home/home';
import MainMenuScreen from '../screens/Home/mainMenu';
import ContactScreen from '../screens/Home/contact';
import politic from '../screens/Home/politic';

import PlaceDetailScreen from '../screens/Places/placeDetail';
import PlaceScreen from '../screens/Places/placesByTaxonomy';

import Icon from 'react-native-vector-icons/MaterialIcons';

const navigationOptions = {
	navigationOptions: {
		headerStyle: {
			backgroundColor: '#1993de'
		},
		contentComponent:search,
		headerTitleStyle: {
			textAlign: 'center',
			alignSelf: 'center',
			fontSize: 20,
			color: 'white',
			fontWeight: 'bold'
		}
	}
};

//el ancho del menu es muy largo y toma la barra de notificaciones, checar eso, 
//falta el hr el menu o la linea debajo de contacto
//falta insertar en el header el cuadro de busqueda
const Custom = (props) => (
	<SafeAreaView>
		<View style={{height: null, width: null, backgroundColor: '#1993de', alignItems:'center'}}>
			<Image source={require('../../assets/logo.jpg')}/>
		</View>
		<ScrollView>
			<DrawerItems {...props}/>
		</ScrollView>
	</SafeAreaView>
)

const search = () => (
	<SafeAreaView>
		<View  style={{height: 200, backgroundColor: 'white'}}>
			<SearchBar
			lightTheme
			onChangeText={someMethod}
			onClearText={someMethod}
			icon={{ type: 'font-awesome', name: 'search' }}
			placeholder='Type Here...' />
			<TextInput
				style={{height: 40, borderColor: 'gray', borderWidth: 1}}
				onChangeText={(text) => this.setState({text})}
				value={this.state.text}
			/>
		</View>
		<ScrollView>
			<DrawerItems {...props}/>
		</ScrollView>
	</SafeAreaView>
)

const leftIcon = (navigation, icon) => <Icon
	name={icon}
	style={{marginLeft: 20}}
	size={30}
	color="white"
	onPress={() => navigation.openDrawer()}
/>;

const rightIcon = (navigation, icon, screen) => <Icon
	name={icon}
	style={{marginLeft: 20}}
	size={30}
	color="white"
	onPress={() => navigation.navigate(screen)}
/>;

const mainMenuStack = createStackNavigator(
	{
		HomeScreen: {
			screen: HomeScreen,
			title: 'Inicio',
			navigationOptions: ({navigation}) => ({
				headerLeft: leftIcon(navigation, 'menu')
			})
		},
		MainMenuScreen: {
			screen: MainMenuScreen,
			title: 'Indice',
			navigationOptions: ({navigation}) => ({
				headerLeft: rightIcon(navigation, 'keyboard-arrow-left', 'HomeScreen'),
			})
		},
		PlacesScreen: {
            screen: PlaceScreen,
            navigationOptions: ({ navigation }) => ({
                title: 'Negocios',
                headerLeft: rightIcon(navigation, 'keyboard-arrow-left', 'MainMenuScreen'),
            })
        },
		PlaceDetailScreen: {
            screen: PlaceDetailScreen,
            navigationOptions: ({ navigation }) => ({
                title: 'Detalle',
                headerLeft: rightIcon(navigation, 'keyboard-arrow-left', 'PlacesScreen'),
            })
        },


	},
	navigationOptions
);


const contactStack = createStackNavigator(
	{
		contactScreen: {
			screen: ContactScreen,
			navigationOptions: ({navigation}) => ({
				headerLeft: leftIcon(navigation, 'menu')
			})
		}
	},
	navigationOptions
);

const privatScreen = createStackNavigator(
	{
		privatScreen: {
			screen: politic,
			navigationOptions: ({navigation}) => ({
				headerLeft: leftIcon(navigation, 'menu')
			})
		}
	},
	navigationOptions
);


//dar estilo a el menu, es decir agregar imagen y fondo a el menu lateral asi como formato a la letra
//limpiar de estilos no usados en los iconos
export default createDrawerNavigator(
	{ 
		MainMenuScreen: {
            screen: mainMenuStack,
            navigationOptions: ({ navigation }) => ({
                drawerLabel: 'Inicio',
                drawerIcon: ({tintColor}) => (<Icon name="home" size={25} style={{color: '#757575'}} />),
              })
        },
        ContactScreen: {
            screen: contactStack,
            navigationOptions: ({ navigation }) => ({
                drawerLabel: 'Contacto', style:{fontSize:40},
                drawerIcon: ({tintColor}) => (<Icon name="phone" size={25} style={{color: '#757575', paddingLeft:2}} />),
              })
		},
        privatScreen: {
            screen: privatScreen,
            navigationOptions: ({ navigation }) => ({				
                drawerLabel: 'Políticas de uso',
                drawerIcon: ({tintColor}) => (<Icon name="lock" size={25} style={{color: '#757575'}} />),
              })
		}
    },
	{
		drawerBackgroundColor : '#ffffff',
		contentComponent: Custom,
		contentOptions: {
			fontSize: 60,
			activeTintColor: '#757575',
			inactiveTintColor : '#757575',
			itemsContainerStyle: {
                marginVertical: 20
			}
		},
	}
)