import {observable, action } from 'mobx';

export default class MenuStore {
    @observable taxonomy = {name: '', slug: ''};
    @observable city = {name: '', slug: ''};

    @action setTaxonomies(taxonomies) {
        this.taxonomies = taxonomies;
    }

    @action setSubTaxonomies(subTaxonomies) {
        this.subTaxonomies = subTaxonomies;
    }

    @action setTaxonomy(taxonomy) {
        this.taxonomy = taxonomy;   
    }

    @action setCity(city) {
        this.city = city;   
    }
}