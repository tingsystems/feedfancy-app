import React from 'react';
import PreLoader from "./application/components/PreLoader";
import firebaseConfig from './application/utils/firebase';
import { observer } from 'mobx-react/native'
import {Provider} from 'mobx-react';
import store from './application/stores'
import * as firebase from 'firebase';
firebase.initializeApp(firebaseConfig);

import GuestNavigation from './application/navigations/guest';

console.disableYellowBox = true;

@observer
export default class App extends React.Component {
	constructor () {
		super();
		this.state = {
			loaded: false
		}
	}

	componentDidMount () {
		this.setState({loaded: true});
	}

	render() {
		const {loaded} = this.state;

		if (!loaded) {
			return (<PreLoader/>);
		}
		return (
			<Provider store={store}>
				<GuestNavigation />
			</Provider>
		);
	}
}


